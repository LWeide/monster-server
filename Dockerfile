FROM node:latest

LABEL maintainer="dominik.brosch@betafase.com"

ADD server.js /app/server.js
ADD package.json /app/package.json
ADD yarn.lock /app/yarn.lock
ADD public/ /app/public
WORKDIR /app
RUN yarn install

ENTRYPOINT ["yarn", "start"]