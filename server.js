const express = require('express')
const app = express()
const port = 80

const { Pool } = require('pg')
const client = new Pool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PW,
    database: process.env.DB,
    port: 5432
});

const queries = {
    all_with_skills: `SELECT jobid, companyheader, companyname, companyindustry, skills, joblocationcountry, datasource, joblocationcity, CASE
    WHEN skills is null then null
    WHEN lower(skills) like 'experience%' THEN 'experience'
    WHEN skills like '{%' then 'json'
    WHEN lower(skills) like 'role%' or lower(skills) like '- role%' THEN 'role'
    ELSE 'unknown' END as typ
    FROM data
    WHERE not skills is null
    ORDER BY typ asc`,
    citylocation: `select count(*), joblocationcountry, joblocationcity
    FROM data
    where not skills is null
    GROUP BY joblocationcountry, joblocationcity
    order by count desc`,
    remaining: `SELECT count(*), datasource FROM data WHERE (jobcategory='IT/Software Development') AND NOT companyindustry IS NULL AND NOT companyindustry = 'Staffing/Employment Agencies' AND skills IS NULL GROUP BY datasource`,
    remaining2: "FROM data WHERE (jobcategory='IT/Software Development') AND NOT companyindustry IS NULL AND NOT companyindustry = 'Staffing/Employment Agencies' AND (skills IS NULL OR NOT (lower(skills) like 'role%' or lower(skills) like '- role%' or lower(skills) like 'experience%'))"
};
app.use(express.static('public'));
app.use(express.json());

app.get("/rest/next", (_, res) => {
    const selector1 = `joblocationcountry= 'US' and NOT (lower(skills) like 'role%' or lower(skills) like '- role%' or lower(skills) like 'experience%')`;
    const selector2 = `datasource = 'monster.com' and joblocationcountry = 'US' and not companyindustry is null and companyindustry != 'Management Consulting Services' and companyindustry != 'Staffing/Employment Agencies' AND NOT companyindustry IS NULL AND NOT companyindustry = 'Staffing/Employment Agencies' AND ((lower(jobdescription) LIKE '%agile%' or lower(jobdescription) LIKE '%scrum%') or lower(companyheader) like '%scrum%') AND (skills IS NULL OR NOT (lower(skills) like 'role%' or lower(skills) like '- role%' or lower(skills) like 'experience%'))`;
    const selector3 = `jobcategory = 'IT/Software Development' AND skills is null and not companyindustry is null and not companyindustry = 'Staffing/Employment Agencies' and companyindustry != 'Management Consulting Services'`;
    const query = "SELECT jobid, companyheader, companyname, companyindustry, jobcategory, jobdescription FROM data WHERE "+selector3+" ORDER BY RANDOM() LIMIT 1"
    client.query(query, (err, result) => err ? res.json({ error: err.toString() }) : res.json(result.rows[0]));
});

app.get("/rest/offer/:id", (req, res) => {
    const query = "SELECT jobid, companyheader, companyname, companyindustry, jobcategory, jobdescription, skills FROM data WHERE jobid = $1";
    client.query(query, [req.params.id], (err, result) => err ? res.json({ error: err.toString() }) : res.json(result.rows[0]));
});

app.post("/rest/save", (req, res) => {
    const { skills, jobid } = req.body;
    const query = "UPDATE data SET skills = $1 WHERE jobid = $2";
    client.query(query, [skills, jobid], error => {
        if (error) {
            console.log(error);
            res.status(400).json({ error: error.toString() });
        } else {
            res.json({ success: true });
        }
    });
});

app.get("/rest/query", (req, res) => {
    const id = req.query.id;
    if (id) {
        if (queries[id]) {
            const query = queries[id];
            client.query(query, (err, result) => {
                if (err) {
                    res.json({ success: false, error: err.toString() });
                } else {
                    res.json({ success: true, result: result });
                }
            });
        } else {
            res.json({ success: false, error: "Unknown query" });
        }
    } else {
        res.json({ success: true, queries: Object.keys(queries) });
    }
});

app.listen(port);